'GINGER_Description Upload Test Cases from Calendar to Test Plan and Test Lab
'GINGER_$CALENDAR_PATH
'GINGER_$CLENDAR_SHEET_NAME
'GINGER_$QC_URL
'GINGER_$QC_USERNAME
'GINGER_$QC_PASSWORD
'GINGER_$QC_DOMAIN
'GINGER_$QC_PROJECT
'GINGER_$QC_TEST_PLAN_PATH
'GINGER_$QC_TEST_LAB_PATH
'GINGER_$QC_INTEGRATION_REQUIRED
'GINGER_$QC_UPLOAD_ALL

'Option Explicit  'Line 10

if WScript.Arguments.Count = 0 then
    WScript.Echo "Missing parameters"
end if

' Your code here
Dim excelFilePath
Dim excelSheetName
Dim strValue
Dim SNO
Dim FoundCell
Dim Data_Var,FoundCell_COLUMN
Dim TestFound
Dim Testid

'Initialize variables 
excelFilePath = WScript.Arguments(0)
excelSheetName = WScript.Arguments(1)
qcServer = WScript.Arguments(2)
qcUser = WScript.Arguments(3)
qcPassword = WScript.Arguments(4)
qcDomain = WScript.Arguments(5)
qcProject = WScript.Arguments(6)
qcTestPlanPath = WScript.Arguments(7)
qcTestLabPath = WScript.Arguments(8)
strQCIntergationReq = WScript.Arguments(9)
strQCUploadAll = WScript.Arguments(10)

'Added by Subodh Singh on 29-Aug-2018
'Add Test Cases in Test Plan and Test Set 
'============================
If strQCIntergationReq = "Y" Then

		If strQCUploadAll = "Y" Then
			Call fDelSkipColumnfromCalendar()
		End If 
			
		Set tdc = CreateObject("TDApiOle80.TDConnection")

        strQCConnection = makeConnection(qcServer,qcUser,qcPassword,qcDomain,qcProject)

		'Check folder exist in Test Lab
		Set objTSTreeManager = tdc.TestSetTreeManager
		Set objTSFolder = objTSTreeManager.NodeByPath(qcTestLabPath)

		'Check folder exist in Test Plan
		Set TestFact = tdc.TestFactory
		Set objTPTreeManager = tdc.TreeManager
		Set objTPFolder =objTPTreeManager.NodeByPath(qcTestPlanPath)
		
		Do While fReadDatafromCalendar("TEST_NAME") <> "none" 
			
			strTestSetRelFolderPath = fReadDatafromCalendar("QC_TEST_SET_RELATIVE_FOLDER")
			objTSFolder = objTSFolder & strTestSetRelFolderPath
			'Read test case and test set name from calendar
			strTestSetName = fReadDatafromCalendar("TEST_SET_NAME")
			strTestCaseName = fReadDatafromCalendar("TEST_NAME")
				
			'Check Test Case exist in Test Plan then retrieve the Test ID, if not retrieve create and retrieve Test ID
			strTCID = fnCreateTCinTestPlan(qcTestPlanPath,strTestCaseName) 
			
			If strTCID = "" Then 
				ReadData = "Something Went Wrong While Test Case Creation in Test Plan!Please check"
				Exit Do
			End If
			
			'Check Test Case Exist in Test Set, if not pull it from Test Plan OR if yes then skip the iteration  
			strStatus = fnCreateTCinTestSet(qcTestLabPath,strTestSetName,strTCID)
			
			If strStatus <> "Test Case Pulled in Test Set" Then 
				ReadData = "Something Went Wrong While Test Case Creation in Test Set!Please check"
				Exit Do
			End If
			
			'Update Skip column 
			Call fReadDatafromCalendar("SKIP")
				
		Loop
		
		If ReadData = "" Then 
			ReadData = "Test Lab Setup Done Successfully!"
		End If 
End If
'=============================

Function makeConnection(qcServer,qcUser,qcPassword,qcDomain,qcProject)

              boolConnected = false
              'Set tdc = CreateObject("TDApiOle80.TDConnection")

              tdc.InitConnectionEx qcServer

              If tdc.Connected Then

                             'Login to QC

                             tdc.Login qcUser,qcPassword

                             If tdc.LoggedIn Then

                                           'Connect to QC Project
                                           tdc.Connect qcDomain, qcProject

                                           If tdc.ProjectConnected Then
                                                          boolConnected = true
                                                          'makeConnection = tdc

                                           Else
                                                          boolConnected = false
                                           End If
                             Else
                                                          boolConnected = false
                             End If
              Else
                             boolConnected = false
              End If

              If boolConnected = true Then
                             makeConnection = true
              Else
                             makeConnection = false
              End If

End Function

Function FileExist(excelFilePath)

	Set objFSO = CreateObject("Scripting.FileSystemObject")
	strFile = objFSO.FileExists(excelFilePath)

	If Not strFile Then
		WScript.Echo "File Not Exist in "& excelFilePath
		Exit Function
	End If

FileExist = True

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Function name: 	fDelSkipColumnfromCalendar()
' Description:  	Get PARAM_VALUE from calendar using PARAM_VALUE
' Parameters:   	PARAM_NAME(Method arguments)
'					excelFilePath, excelSheetName (Global Parameters)
' Return value:   	Success - True
' Failure - False
' Author:          	Subodh Singh
' Date:				31 AUG 2018
' Updated:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function fDelSkipColumnfromCalendar()

	'Check File Exist
	If FileExist(excelFilePath) = False Then
		WScript.Echo "File Not Exist in "& excelFilePath
		fReadDatafromDataFile = "File Not Exist in "& excelFilePath
		Exit Function
	End If

	'Create Excel Objects
	Set objXls = CreateObject("Excel.Application")
	Set objWBook = objXls.Workbooks.Open(excelFilePath)
	Set objWSheet = objWBook.Worksheets(excelSheetName)

	'Check row exist with name "END" in column B
	Set FoundCell = objWSheet.Range("B1:B20000").Find("END")

	'Store last valid row number in variable - strValue
	If Not FoundCell Is Nothing Then
  		strValue =  FoundCell.Row
	End If
	
	objWSheet.Range("B2:B"&strValue-1).ClearContents
			
	objWBook.Save
	objWBook.Close
	objXls.Quit
	
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Function name: 	fReadDatafromCalendar()
' Description:  	Get PARAM_VALUE from calendar using PARAM_VALUE
' Parameters:   	PARAM_NAME(Method arguments)
'					excelFilePath, excelSheetName (Global Parameters)
' Return value:   	Success - True
' Failure - False
' Author:          	Subodh Singh
' Date:				31 AUG 2018
' Updated:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function fReadDatafromCalendar(PARAM_NAME)
	'Check File Exist
	If FileExist(excelFilePath) = False Then
		WScript.Echo "File Not Exist in "& excelFilePath
		fReadDatafromDataFile = "File Not Exist in "& excelFilePath
		Exit Function
	End If

	'Create Excel Objects
	Set objXls = CreateObject("Excel.Application")
	Set objWBook = objXls.Workbooks.Open(excelFilePath)
	Set objWSheet = objWBook.Worksheets(excelSheetName)

	'Check row exist with name "END" in column B
	Set FoundCell = objWSheet.Range("B1:B20000").Find("END")

	'Store last valid row number in variable - strValue
	If Not FoundCell Is Nothing Then
  		strValue =  FoundCell.Row
	End If

	'Store cell having empty SKIP column
    Set FoundCell_Empty = objWSheet.Range("B1:B" & strValue).Find("")
	
	If FoundCell_Empty Is Nothing Then
		fReadDatafromCalendar = "none"
		'Save excel objects
		objWBook.Save
		objWBook.Close
		objXls.Quit
		Exit Function
	End If
	'On Error Resume Next
	'Store cell value having column name in found cells
	Set FoundCell = objWSheet.Range("A" & FoundCell_Empty.Row & ":BZ" & FoundCell_Empty.Row).Find(PARAM_NAME, , , 1)
	
	If PARAM_NAME="SKIP" Then	
		PARAM_VALUE = "N"
		objWSheet.Cells(FoundCell_Empty.Row,2).Value="X"
		objWSheet.Cells(FoundCell_Empty.Row+1,2).Value=PARAM_VALUE
	End If
	
	'Get PRARAM_VALUE for given column PARAM_NAME
	If Not FoundCell Is Nothing Then
		If Not FoundCell_Empty Is Nothing Then
			If PARAM_NAME="SKIP" Then
				objWSheet.Cells(FoundCell_Empty.Row,2).Value="X"
				objWSheet.Cells(FoundCell_Empty.Row+1,2).Value=PARAM_VALUE
			Else
				PARAM_VALUE = objWSheet.Cells(FoundCell.Row+1,FoundCell.Column).Value
			End If
		Else
			PARAM_VALUE = "none"
		End If
	End If

	'Save excel objects
	objWBook.Save
	objWBook.Close
	objXls.Quit

	'Clear excel objects
	Set objWSheet = Nothing
	Set objXls = Nothing
	Set FoundCell = Nothing
	set Copyrange = Nothing

	'Return PARAM_VALUE
	'msgbox PARAM_VALUE
	fReadDatafromCalendar = PARAM_VALUE

End Function

'=================================================================
Public Function fnCreateTCinTestPlan(strTPPath,strTCName)
	
	Dim strTCID
	strTCID = 0
	
	Set TestFact = tdc.TestFactory
	Set MyTMgr = tdc.TreeManager
	Set objTPFolder = MyTMgr.NodeByPath(strTPPath) 
	
	strTestFactoryFilter = "select TS_TEST_ID from TEST where TS_NAME = '" & strTCName & "' and TS_SUBJECT = " & objTPFolder.NodeID
	Set objTestList = TestFact.NewList(strTestFactoryFilter)
	
	
	'VerIfy If the test in the test plan otherwise it creates the test in the test plan 
	If objTestList.Count = 0 Then
		'msgbox objTestList.Count
		Set MyTest = TestFact.AddItem(null)
		MyTest.Name = strTCName
		MyTest.Field("TS_DESCRIPTION") = strTCName
		MyTest.Field("TS_SUBJECT") = objTPFolder.NodeID
		Call fAdditionalFieldsForCreatingQCTest(MyTest)
		MyTest.Post
		strTCID =  MyTest.id
	End If	
	
	'Retrieve Test case id if Test Case already exist in Test Plan 
	If objTestList.Count > 0 Then
		
		Set objTest = objTestList.Item(1)
		strTCID = objTest.id
	End If
	
	fnCreateTCinTestPlan = strTCID
	
End Function 

'=================================================================
Public Function fnCreateTCinTestSet(strTSFolderPath,strTestSetName,strTCID)

	Set TSTestFact = tdc.TestSetFactory
	Set objTSTreeManager = tdc.TestSetTreeManager
	
	Set objTSFolder = objTSTreeManager.NodeByPath(strTSFolderPath)
	
	If objTSFolder Is Nothing Then  
        'msgbox "Path Not Found."
    Else
        'Msgbox "Path Found"
    End If
	
	' Search for the test set passed as an argument to the example code
	
    Set tsList = objTSFolder.FindTestSets(strTestSetName)
    '----------------------------------Check if the Test Set Exists --------------------------------------------------------------------
    If tsList Is Nothing Then
        fnCreateTCinTestSet = "Test Set "&strTestSetName&" not found in folder." 
    
	Else 
		bFlag = false 
		counter = 1
		
			For Each TestSet in tsList
				
				If TestSet.Name = strTestSetName Then 
					bFlag = true
					Exit For 
				End If
				
				counter = counter + 1
			Next
		
		
		If tsList.count > 0 and bFlag = true Then 
			
			Set objTestSet = tsList.Item(counter)
			Set TestSetF = objTestSet.TSTestFactory

			'Set qcCon=tdc.QCConnection
			'Set qcCom=tdc.Command
			strTestFactoryFilter = "SELECT TC_Test_ID  FROM TESTCYCL WHERE tc_test_id = " & strTCID  & " and TC_CYCLE_ID = " & objTestSet.ID
			Set objTestList = TestSetF.NewList(strTestFactoryFilter)
			
			'msgbox objTestList.Count
			
			'If Test Case not exist in Test Set, pull it from Test Plan
			If objTestList.Count = 0 Then
				
				Set MyTest = TestSetF.addItem(strTCID)
				MyTest.Status ="No Run"
				MyTest.Post
				fnCreateTCinTestSet = "Test Case Pulled in Test Set"
				Exit Function
			End If
		Else 
			
			fnCreateTCinTestSet = "Something went wrong."
		End If 
	End If 
	
	fnCreateTCinTestSet = "Something went wrong."
	
End Function 
'=================================================================
' NOTE- Please Update Additional Parameter Required for your QC Project in below Function 
'=================================================================
Function fAdditionalFieldsForCreatingQCTest(MyTest)

	'MyTest.Field("TS_TYPE") = "MANUAL"
	'MyTest.Field("TS_USER_TEMPLATE_15") = "TIS"
	'MyTest.Field("TS_USER_TEMPLATE_05") = "BSS 9.2"
	'MyTest.Field("TS_USER_TEMPLATE_07") = "BSS Migration"
	'MyTest.Field("TS_USER_TEMPLATE_01") = "01-Design & Development"
	'MyTest.Field("TS_USER_TEMPLATE_10") = "04-SIT - Integration Test"
	'MyTest.Field("TS_USER_TEMPLATE_19") = "Bill Pay - Drop2"
	'MyTest.Field("TS_USER_TEMPLATE_12") = "CHQ CRM"
	'MyTest.Field("TS_USER_TEMPLATE_13") = "COP - Data Setup"

End Function 
'=================================================================
' output results to Ginger
Wscript.echo "~~~GINGER_RC_START~~~"
WScript.Echo ReadData
'Wscript.echo strVariable & "=" + ReadData
Wscript.echo "~~~GINGER_RC_END~~~"
