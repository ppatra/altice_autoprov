'GINGER_Description Renamefile
'GINGER_$excelFilePath
'GINGER_$excelSheetName


'Option Explicit  'Line 10

if WScript.Arguments.Count = 0 then
    WScript.Echo "Missing parameters"        
end if

' Your code here
Dim excelFilePath
Dim excelSheetName
Dim strValue
Dim SNO
Dim FoundCell 
Dim Data_Var,FoundCell_COLUMN

excelFilePath = WScript.Arguments(0)
excelSheetName = WScript.Arguments(1)




'############################################################

' Function name: FileExist

' Description:   

' Return value:  Success - True , Fail - False                           


'#############################################################

Function FileExist(excelFilePath)
	
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	strFile = objFSO.FileExists(excelFilePath)
	
	If Not strFile Then
		WScript.Echo "File Not Exist in "& excelFilePath
		Exit Function
	End If

FileExist = True

End Function

'############################################################

' Function name: fReadDatafromDataFile

' Description:   

' Parameters:    None

' Return value:  Success - True , Fail - False                           



'#############################################################

 Function fReadDatafromDataFile(excelFilePath)
	

	If FileExist(excelFilePath) = False Then
		WScript.Echo "File Not Exist in "& excelFilePath		
		fReadDatafromDataFile = "File Not Exist in "& excelFilePath
		Exit Function
	End If

	
   Set objXls = CreateObject("Excel.Application")
   Set objWBook = objXls.Workbooks.Open(excelFilePath)
   Set objWSheet = objWBook.Worksheets(excelSheetName)
	
			
	Set FoundCell = objWSheet.Range("B1:B20000").Find("END")	
	
	If Not FoundCell Is Nothing Then
  		strValue =  FoundCell.Row          
	
	End If
	
	Set FoundCell_COLUMN = objWSheet.Range("A1:AZ1").Find("PARAM0")
	
	
	If Not FoundCell_COLUMN Is Nothing Then
		Wscript.Echo "PARAM01 Doesn't found!"
	End If

        	
	Set FoundCell = objWSheet.Range("B1:B"&strValue).Find("")
	

	If Not FoundCell Is Nothing Then

		'SNO = objWSheet.Cells(FoundCell.Row,1)
	  'Msgbox FoundCell.Row
		
       	  For j = FoundCell_COLUMN.Column  To 500
		
	

                If Trim(objWSheet.Cells(FoundCell.Row, j))<>"" then
		  If Trim(objWSheet.Cells(FoundCell.Row + 1, j))="" Then
			
			Conc_cnt="Empty"
		Else		
			Conc_cnt=Trim(objWSheet.Cells(FoundCell.Row + 1, j))
		 End If		
					   	
	           Data_Var = Data_Var & Trim(objWSheet.Cells(FoundCell.Row, j)) & "," & Conc_cnt & ","
			

		Else 
			Exit For
        	End if        
	  Next
        
        	Data_Var = Left(Data_Var, Len(Data_Var) - 1)

	        'fReadDatafromDataFile = Data_Var
		SNO=Data_Var

	Else

		SNO = "none"

	End If
	


	objWBook.Save
	
	objWBook.Close

	objXls.Quit
	
	Set objWSheet = Nothing
	Set objXls = Nothing
	Set FoundCell = Nothing
	set Copyrange = Nothing

fReadDatafromDataFile = SNO
 End Function

'*************************************************************************************************************************************************

Call FileExist(excelFilePath)

 ReadData = fReadDatafromDataFile(excelFilePath)



' output results to Ginger
Wscript.echo "~~~GINGER_RC_START~~~" 
WScript.Echo ReadData 
'Wscript.echo strVariable & "=" + ReadData 
Wscript.echo "~~~GINGER_RC_END~~~"














